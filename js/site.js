$().ready( function(){

//slider
if ($(".slide").length > 0){
    $(document).ready(function(){
        $('.slide').bxSlider({
            auto: true,
            mode: 'fade',
            adaptiveHeight: false,
            touchEnabled:true,
            controls:false,
            speed:500,
            pause:7000
        });
    });
}
    //mobile menu show/hide
    if ($(".menuimg").length > 0){
        $(".menuimg").click(function(){
            $('#nav').first().slideToggle();
            $('.menuimg').toggleClass('open');
        });
    }


});