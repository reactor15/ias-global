<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

 /*
 
company_name
street
town_city
county_state_region
Postcode_zipcode
country
telephone
mailfrom
website
iata_number
contact_name
position
//x:36
//y:6
mailto:marketing@iasglobal.com
html:y
subject:IAS website enquiries form
use_names:y
show_name:y
show_email:y
require:company_name,telephone,mailfrom,website,contact_name,position
thanks:http://www.ias-global.com/thanks.html
opps:http://www.ias-global.com/oops.html
seccode:HjTn7HgaY
blckk: */
 
 
if(isset($_POST['mailfrom'])) { 

  
 
    $email_to = "marketing@iasglobal.com";
   //    $email_to = "james@reactor15.com";
    $email_subject = "IAS website enquiries form ";
 
    function died($error) {
        header("Location: https://www.ias-global.com/oops.html");
     //   echo $error."<br /><br />";
        exit;
    }
//message email iata business name
    if(!isset($_POST['company_name']) ||
 
        //!isset($_POST['address']) ||
 
        //!isset($_POST['Postcode']) ||

        !isset($_POST['telephone']) ||
        !isset($_POST['mailfrom']) ||
        !isset($_POST['website']) ||
        !isset($_POST['contact_name']) ||

      //  !isset($_POST['telephone']) ||
 
        !isset($_POST['position'])) {

        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 
    $company_name = $_POST['company_name']; // required
    $street = $_POST['street']; // required
    $town_city = $_POST['town_city']; // required
    $county_state_region = $_POST['county_state_region']; // requiredcountry
        $country = $_POST['country']; // requiredcountry
    $Postcode_zipcode = $_POST['Postcode_zipcode']; // required
	$telephone = $_POST['telephone']; // required
    $mailfrom = $_POST['mailfrom']; // required
    $website = $_POST['website']; // required
    $iata_number = $_POST['iata_number']; // required
    $contact_name = $_POST['contact_name']; // required
    $position = $_POST['position']; // required

    $iata = $_POST['iata']; // not required

    $business = $_POST['business']; // not required

   // $requirements = $_POST['requirements']; // required
 
     
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$mailfrom)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$contact_name)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }
  /*
  if(strlen($requirements) < 2) {
 
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
 
  }*/
 
  if(strlen($error_message) > 0) {

    died($error_message);

  }
 
    $email_message = "Website form details below:\n\n";

    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }


/*

$company_name
$street
$town_city
$county_state_region
$Postcode_zipcode
$country
$telephone
$mailfrom
$website
$iata_number
$contact_name
$position

*/


    $email_message .= "Company name: ".clean_string($company_name)."\n";
    $email_message .= "Street: ".clean_string($street)."\n";
    $email_message .= "Town city: ".clean_string($town_city)."\n";
	$email_message .= "County state region: ".clean_string($county_state_region)."\n";
	$email_message .= "Postcode_zipcode: ".clean_string($Postcode_zipcode)."\n";
	$email_message .= "Country: ".clean_string($country)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
	$email_message .= "Mail from: ".clean_string($mailfrom)."\n";
	$email_message .= "Website: ".clean_string($website)."\n";
	$email_message .= "Iata number: ".clean_string($iata_number)."\n";
	$email_message .= "Contact name: ".clean_string($contact_name)."\n";
	$email_message .= "Position: ".clean_string($position)."\n";
     
 
// create email headers
 $email_from = 'no-reply@ias-global.com';
$headers = 'From: '.$email_from."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
    // aptly named honeypot
    if(!$_POST['subject']) {
        @mail($email_to, $email_subject, $email_message, $headers);
    }

header("Location: https://www.ias-global.com/thanks.html");
 
}

//header("Location: http://skyteammarineandoffshore.com/oops.html");
     //   echo $error."<br /><br />";
//exit;

?>