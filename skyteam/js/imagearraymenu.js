// Set up Image Array here from the root of the website
// Only add Mouse Over images to the array.
imgsrc = new Array();
imgsrc[0] = "images/czech-airlines_o.gif";
imgsrc[1] = "images/m_examples_o.gif";
imgsrc[2] = "images/m_processes_o.gif";
imgsrc[3] = "images/m_catalogue_o.gif";
imgsrc[4] = "images/m_contact_o.gif";
/* 
Usage on Roll Overs:

onMouseOver="imgChgOver('NUMBER_FROM_ARRAY','NAME_OF_IMAGE');"
onMouseOut="imgChgOut('NUMBER_FROM_ARRAY','NAME_OF_IMAGE');"

*/
////////////////////////////////////////////////
//		Image Array 
//		�2005 Reactor
//		www.reactor15.com
///////////////////////////////////////////////
//	Do Not Alter anything below here.....
////////////////////////////////////////////////
function getAppVersion() 
{
	if ( (navigator.appName == "Netscape") && ( navigator.appVersion.substring(0, 1) >= 3 ) ) return 1;
	if ( (navigator.appName == "Microsoft Internet Explorer") && (navigator.appVersion.substring(0, 1) >= 4) ) return 1;
	return 0;
}

function imgChgOver(num, imgname) 
{
	if (getAppVersion()) 
	{
		imgOut[num] = new Image();
		imgOut[num].src = document[imgname].src
 		document[imgname].src = img[num].src;
	}
}

function imgChgOut(num, imgname)
{
	if (getAppVersion()) 
	{
 		document[imgname].src = imgOut[num].src;
	}
}

var path = "";
if (location.href.indexOf("/html/") != -1)
{
	path = "../";
}

function LoadImgs()
{
	if (getAppVersion()) {
		imgOut = new Array();
        img = new Array();
        for (i = 0; i < imgsrc.length; i++) {
                img[i] = new Image();
                img[i].src = path + imgsrc[i];
        }
	}
}

LoadImgs();